import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ContaDetalhesPageComponent } from './conta-detalhes-page.component';
import { AccountsModule } from 'src/app/accounts/accounts.module';

@NgModule({
    declarations: [ContaDetalhesPageComponent],
    imports: [BrowserModule, BrowserAnimationsModule, AccountsModule],
    providers: [],
    exports: [ContaDetalhesPageComponent],
})
export class ContaDetalhesModule {}
