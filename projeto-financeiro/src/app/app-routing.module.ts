import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContaDetalhesPageComponent } from './pages/conta-detalhes-page/conta-detalhes-page.component';
import { AppLayoutComponent } from './core/app-main-layout/app-layout/app-layout.component';

const routes: Routes = [
    {
        path: 'app',
        component: AppLayoutComponent,
        children: [
            {
                path: 'movimentacoes',
                component: ContaDetalhesPageComponent,
            },
            {
                path: 'teste',
                component: AppLayoutComponent,
            },
        ],
    },
    { path: '', redirectTo: 'app/movimentacoes', pathMatch: 'full' },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
