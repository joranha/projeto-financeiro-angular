import { Component, OnInit, Input } from '@angular/core';
import { MovimentacaoApiModel } from '../../models/movimentacao-api-model';

@Component({
    selector: 'app-movimentacao-list',
    templateUrl: './movimentacao-list.component.html',
    styleUrls: ['./movimentacao-list.component.css'],
})
export class MovimentacaoListComponent implements OnInit {
    @Input() movimentacoes: MovimentacaoApiModel[];

    constructor() {}

    ngOnInit() {}
}
