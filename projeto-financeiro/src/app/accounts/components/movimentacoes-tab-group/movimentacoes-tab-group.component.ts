import { Component, OnInit } from '@angular/core';
import { MovimentacaoApiModel } from '../../models/movimentacao-api-model';
import { combineLatest } from 'rxjs';
import { MovimentacaoService } from '../../services/movimentacao.service';

@Component({
    selector: 'app-movimentacoes-tab-group',
    templateUrl: './movimentacoes-tab-group.component.html',
    styleUrls: ['./movimentacoes-tab-group.component.css'],
})
export class MovimentacoesTabGroupComponent implements OnInit {
    entrada: MovimentacaoApiModel[];
    aberto: MovimentacaoApiModel[];
    pago: MovimentacaoApiModel[];
    atrasado: MovimentacaoApiModel[];

    constructor(private movimentacaoSevice: MovimentacaoService) {}

    ngOnInit() {
        this.initMovimentacoes();
    }

    initMovimentacoes() {
        combineLatest(
            this.movimentacaoSevice.getEntradas(),
            this.movimentacaoSevice.getSaidas()
        ).subscribe(([entradas, saidas]) => {
            this.entrada = entradas;
            const { aberto, pago, atrasado } = saidas;
            this.aberto = aberto;
            this.pago = pago;
            this.atrasado = atrasado;
        });
    }
}
