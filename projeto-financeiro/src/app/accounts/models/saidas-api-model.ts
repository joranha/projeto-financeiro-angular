import { MovimentacaoApiModel } from './movimentacao-api-model';

export interface SaidasApiModel {
    atrasado: MovimentacaoApiModel[];
    pago: MovimentacaoApiModel[];
    aberto: MovimentacaoApiModel[];
}
