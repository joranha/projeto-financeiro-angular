import { NgModule } from '@angular/core';
import { CommonModule, NgClass } from '@angular/common';
import { MovimentacaoComponent } from './components/movimentacao/movimentacao.component';
import { MovimentacaoService } from './services/movimentacao.service';
import { HttpClientModule } from '@angular/common/http';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material';
import { MovimentacaoListComponent } from './components/movimentacao-list/movimentacao-list.component';
import { MovimentacoesTabGroupComponent } from './components/movimentacoes-tab-group/movimentacoes-tab-group.component';

@NgModule({
    declarations: [
        MovimentacaoComponent,
        MovimentacaoListComponent,
        MovimentacoesTabGroupComponent,
    ],
    imports: [
        CommonModule,
        HttpClientModule,
        MatToolbarModule,
        MatCardModule,
        MatTabsModule,
    ],
    exports: [MovimentacoesTabGroupComponent],
    providers: [MovimentacaoService],
})
export class AccountsModule {}
