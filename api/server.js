// server.js
const fs = require('fs');
const cors = require('cors');
const bodyParser = require('body-parser');
const jsonServer = require('json-server');

const server = jsonServer.create();
const router = jsonServer.router('db.json');

server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());
server.use(cors());

server.get('/entradas', (req, res, next) => {
    console.log(req.body);

    res.status(200).json({
        entradas: db.movimentacao,
    });
    return;
});

server.use(router);
server.listen(3333, () => {
    console.log('JSON Server is running');
});
